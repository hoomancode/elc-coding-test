"use strict";
/**
 * 
 * File is used to setup the General project
 *  
 */

module.exports = {    

    development_folder: ".local_server",

    production_folder: "prod",

    image_folder: "img",

    hostname: "localhost",

    server_port: 3035,

    dev_port: 3030

};
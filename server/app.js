/**
 * The Server Can be configured and created here...
 * 
 * You can find the JSON Data file here in the Data module. Feel free to impliment a framework if needed.
 */

/*
-- This is the product data, you can view it in the file itself for more details 
{
    "_id": "019",
    "isActive": "false",
    "price": "23.00",
    "picture": "/img/products/N16501_430.png",
    "name": "Damage Reverse Thickening Conditioner",
    "about": "Dolor voluptate velit consequat duis. Aute ad officia fugiat esse anim exercitation voluptate excepteur pariatur sit culpa duis qui esse. Labore amet ad eu veniam nostrud minim labore aliquip est sint voluptate nostrud reprehenderit. Ipsum nostrud culpa consequat reprehenderit.",
    "tags": [
        "ojon",
        "conditioner"
    ]
}
*/
const config    = require('../config/general.config'); // moved 'hostname' and 'port' configs for easier reuse
const data      = require('./data');
const http      = require('http');
// const hostname  = 'localhost';
// const port      = 3035;

// http options
const options = {
  hostname: config.hostname,
  port: config.server_port,
  path: '/search',
  method: 'GET'
}

// handle requests
let req = http.request(options, function(res) {
  res.setEncoding('utf8');
  res.on('data', function (chunk) {
    req.destroy(); // prevent pooling issues, res.end() and agent: false not working
  });
});
// handle errors
req.on('error', function(e) {
  req.destroy();
});
// output
req.write('data\n');
req.end();

// util file for handling requests
const utils     = require('./utils');

/** 
 * Start the Node Server Here...
 * 
 * The http.createServer() method creates a new server that listens at the specified port.  
 * The requestListener function (function (req, res)) is executed each time the server gets a request. 
 * The Request object 'req' represents the request to the server.
 * The ServerResponse object 'res' represents the writable stream back to the client.
 */
http.createServer(function (req, res) {
    // .. Here you can create your data response in a JSON format
    
    // simple router/handler
    const router = {
      'GET/search': utils.searchProducts,
      'default': utils.defaultResponse
    };
    
    let baseURL = `http://${config.hostname}:${config.server_port}/`;
    let reqUrl = new URL(req.url, baseURL);

    // find the method & path, to run its request handler or default
    let routeHandler = router[req.method + reqUrl.pathname] || router['default'];
    routeHandler(req, res, reqUrl);    

    // res.write("Response goes in here..."); // Write out the default response
    // res.end(); //end the response

}).listen(config.server_port); 

console.log(`[Server running on ${config.hostname}:${config.server_port}]`);
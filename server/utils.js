const config = require('../config/general.config');
const data   = require('./data');

const headers = {
  'access-control-allow-origin': `http://${config.hostname}:${config.dev_port}`,
  'access-control-allow-methods': 'GET',
  'access-control-allow-headers': 'content-type, accept',
  'access-control-max-age': -1,
  'Cache-Control': 'no-cache',
  'Content-Type': 'application/json'
};

exports.defaultResponse = (req, res) => {
  res.writeHead(404, headers);
  res.end(JSON.stringify('Oops!  No response found to handle this route...'));
}

exports.searchProducts = (req, res) => {

  let baseURL = `http://${config.hostname}:${config.server_port}/`;
  let reqUrl = new URL(req.url, baseURL);
  let queryStr = "";
  let matches = [];
  
  if( reqUrl.searchParams.get('query') ){
    let searchQuery = reqUrl.searchParams.get('query');
    if( searchQuery.length > 2 ){
      queryStr = searchQuery.toLowerCase();
      data.map(item => {
        if( item.name.toLowerCase().includes(queryStr) ){
          let suggProduct = {
            _id: item._id,
            name: item.name,
            picture: item.picture
          };
          let itemAbout = item.about.slice(0, 40);
          // itemAbout.pop();
          suggProduct.about = itemAbout;
          matches.push(suggProduct);
        }
      });
      res.writeHead(200, headers);
      res.end(JSON.stringify(matches));
    }
  }

  res.writeHead(404, headers);
  res.end();
  req.destroy();
}
# ELC Coding Solution Notes

## Summary

The purpose of this document is to explain my assumptions and approach to the solution.  Given that I'm not including the log of git commits with the project, this document may be helpful for explaining details or reasoning that may not be evident from my code comments.


### Assumptions & Approach (Less Code is Better)

For this project, I pretended as though I am being asked to develop a feature within the context of your development stack.  In the README, you stated:

> You can change the code to match your own preferences however you like. Feel free to change the setup, code or approach however you like

In my experience, I've noticed that some developers get tripped up when working in coding environments that are 'less than ideal' because a large majority only know how to do things in their preferred way.  While having expertise in a particular tool or coding language is helpful, I believe it's more important to focus on context and customer experience requirements to drive engineering decisions.  

To show my versatility as a developer, I chose to make minimal out of scope changes, based on this real-world assumed scenario. 

This is why I specifically chose not to create a separate autocomplete component or to make much structural changes to the project.  

I understand that this is a trade-off for a coding test, because such additional changes would reveal more of my personal coding preferences, but hopefully this approach is appreciated in itself.  Perhaps some of my minimalistic coding and stylistic UI preferences will be evident in the overall code delivered.

### Overview of Autocomplete Code & Project Changes

#### Backend (Server-Side)

I chose to filter the results on the server-side, rather than on the client-side.  I created a very simple Node router/controller setup to handle the requests.  Please see **server/app.js** and **server/utils.js** for this code.

One modification I made on the backend was to put the hostname and port into the general config file, such that I'd be able to easily reuse that info across different files (namely also in **server/utils.js**)

#### Frontend (React UI)

As noted above, I put all the code into the existing files, choosing not to create a separate Autocomplete component.  However, modularizing this element and its sub parts would be a simple next step.  There are other things that could be modularized better such as the header nav and overall layout.

I chose to add **axios** for simplicity of making requests from React.  

As far as the UI, I decided to mimic what I saw on maccosmetics.com, but with minimal CSS.  I also noticed that the UI for the autocomplete results on maccosmetics.com is broken in mobile viewports (it's not responsive).

For my version, I used a flexbox layout and I have provided a simple way to fix that layout issue present on your current site.  I did not spend a lot of time on the styling, but my goal was to take a mobile-first approach and build something functionally and visually usable.

There is also no error or message when results are not found on maccosmetics.com live autocomplete.  It simply shows a blank box.  I have a different approach of providing a message, such that the user will know that their search returned no results.  These autocompleted elements are also dynamically displayed.

Lastly, I made some updates to the CSS and added some responsiveness to the overall layout and menu.  I tried to mimic what is on maccosmetics.com, but again with very minimal CSS.

#### Other / Misc.

Besides axios, the only other package modification is that I added '@babel/plugin-proposal-class-properties' to the package.json, to support adding properties to classes.  This was to workaround an error I was running into earlier.

While I was careful not to make any structural changes to the project, I did optimize the images with ImageOptim and ImageAlpha, greatly reducing their sizes all by roughly 75%.  I can see that on your production site you're using modern image delivery techniques, which is a good idea.  I also optimized the images simply because initially, based on the project instructions, I had assumed that I would need to 'send' the files over and wanted to avoid sending an 8MB file.  However, even now that it's provided in the form of a git repository, it helps to take up less space ;)

-------

### Thank You

I really enjoyed this coding test!
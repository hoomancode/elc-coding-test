/**
 * This file will hold the Menu that lives at the top of the Page, this is all rendered using a React Component...
 * 
 */
import React, { Component } from "react";
import axios from 'axios';

class Menu extends React.Component {

    /**
     * Main constructor for the Menu Class
     * @memberof Menu
     */
    constructor() {
        super();
        this.state = {

            searchPlaceholder: 'Enter Search Term',

            showingMobileMenu: false,

            // Show/hide the search dropdown
            showingSearch: false,

            // Based on user input, products that match
            suggestedResults: [],

            // Show/hide suggestions, depending on if they exist
            showSuggestions: false,

            // User search input
            userInput: ""
        };
    }

    /**
     * Shows or hides the mobile side navigation menu
     * @memberof Menu
     * @param e [Object] - the event from a click handler
     */
    showMobileNav(e) {
        e.preventDefault();
        this.setState({
            showingMobileMenu: !this.state.showingMobileMenu,
            showingSearch: false
        });
    }    

    /**
     * Shows or hides the search container
     * @memberof Menu
     * @param e [Object] - the event from a click handler
     */
    showSearchContainer(e) {
        e.preventDefault();
        this.setState({
            showingSearch: !this.state.showingSearch,
            showingMobileMenu: false
        });
    }

    /**
     * Calls upon search change
     * @memberof Menu
     * @param e [Object] - the event from a text change handler
     */
    onSearch(e) {
        
        // Start Here
        // ...
        e.preventDefault();
        const userInput = e.currentTarget.value;
        this.setState({
          userInput: e.currentTarget.value,
          suggestedResults: []
        });
  
        let suggestedResults = [];
  
        axios.get(`http://localhost:3035/search?query=${userInput}`)
        .then(response => {
          suggestedResults = response.data;
          this.setState({ suggestedResults, showSuggestions: true });
        })
        .catch(err => {
          this.setState({ suggestedResults: [], showSuggestions: false });
        })        
    }

    /**
     * Calls upon 'Enter' and 'Backspace' to reset search
     * @memberof Menu
     * @param e [Object] - the event from a key down handler 
     */
    resetSuggestions(e) {
        const { showSuggestions, suggestedResults, userInput } = this.state;

        if (e.keyCode === 13 || e.keyCode === 8) {
            this.setState({
                showSuggestions: false,
                suggestedResults: [],
                userInput: e.currentTarget.value
            });
        }
    }

    /**
     * Renders the default app in the window, we have assigned this to an element called root.
     *  
     * @returns JSX
     * @memberof App
    */
    render() {
        const {
            onSearch,
            resetSuggestions,
            state: {
                suggestedResults,
                showSuggestions,
                userInput
            }
        } = this;
        
        // dynamically generate a subcomponent for suggestions to show only if there's results
        let suggestedResultsComponent;
        let fullResultsLinkComponent;

        // actual list of suggestions
        let suggList = [];

        if (showSuggestions && userInput) {

            if (suggestedResults.length) {
                
                let suggCount = "";

                if( suggestedResults.length > 4 ){
                    suggCount = 'Displaying 4 of '+suggestedResults.length+' Results';
                    fullResultsLinkComponent = (
                        <span className="suggestions-more">See All Results</span>
                    )
                } else {
                    suggCount = 'Displaying '+suggestedResults.length+' Results';
                }
        
                this.state.suggestedResults.slice(0,4).map((sugg, index) => {
                    suggList.push(
                        <div className="suggestion-item" key={index}>
                            
                            <div className="suggestion-item-image">
                                <img src={'.'+sugg.picture} alt="" className="suggestion-item-image-tag" />
                            </div>
                            <div className="suggestion-item-text">
                                <h5 className="suggestion-item-name">{sugg.name}</h5>
                                <p className="suggestion-item-description">{sugg.about}</p>
                            </div>
                        </div>
                    );    
                });
  
                suggestedResultsComponent = (
                    <div className="suggestions">
                        <div className="suggestions-info">
                            <span className="suggestions-count">{suggCount}</span>
                            {fullResultsLinkComponent}
                        </div>
                        <div className="suggestions-list">
                            {suggList}
                        </div>
                    </div>
                );
  
            } else {
  
                suggestedResultsComponent = (
                    <div className="suggestions no-results">
                        <div className="suggestions-list">
                            <em>No matching products, try another term!</em>
                        </div>
                    </div>
                );
  
            }
        }

        return (
            <header className="menu">
                <div className="menu-container">
                    <div className="menu-holder">
                        <a href="#" className={(this.state.showingMobileMenu ? "active " : "") + "nav-btn-icon nav-btn-menu-toggle"} onClick={(e) => this.showMobileNav(e)}>
                            <i className="material-icons menu-toggle-icon">menu</i>
                        </a>
                        <h1>ELC</h1>
                        <nav className={(this.state.showingMobileMenu ? "active " : "") + "menu"} >
                            <a href="#" className="nav-item">HOLIDAY</a>
                            <a href="#" className="nav-item">WHAT'S NEW</a>
                            <a href="#" className="nav-item">PRODUCTS</a>
                            <a href="#" className="nav-item">BESTSELLERS</a>
                            <a href="#" className="nav-item">GOODBYES</a>
                            <a href="#" className="nav-item">STORES</a>
                            <a href="#" className="nav-item">INSPIRATION</a>
                        </nav>
                        <a href="#" className={(this.state.showingSearch ? "active " : "") + "nav-btn-icon nav-btn-search-toggle"} onClick={(e) => this.showSearchContainer(e)}>
                            <i className="material-icons search-toggle-icon">search</i>
                        </a>
                    </div>
                </div>

                <div className={(this.state.showingSearch ? "showing " : "") + "search-container"}>
                    <input aria-describedby="searchHelp" type="text" value={this.state.userInput} onChange={(e) => this.onSearch(e)} onKeyDown={(e) => this.resetSuggestions(e)} placeholder={this.state.searchPlaceholder}/>
                    <a href="#" onClick={(e) => this.showSearchContainer(e)}>
                        <i className="material-icons close">close</i>
                    </a>
                    {suggestedResultsComponent}
                </div>

            </header>
        );
    }


}

// Export out the React Component
module.exports = Menu;